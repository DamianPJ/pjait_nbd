var mapFun = function() {
	emit(this.job, 0);
};

var reduceFun = function(key, value) {
  return null;
}

db.people.mapReduce( 
   mapFun, 
   reduceFun,
   { 
		out: "jobs"
   }
);

printjson(db.jobs.find({}, {value: 0}).toArray())