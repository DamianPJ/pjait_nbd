var mapFun = function() {
	emit(this.sex, {totalHeight: this.height, totalWeight: this.weight, count: 1});
};

var reduceFun = function(key, values) {
	reducedVal = { totalWeight: 0, totalHeight: 0, count: 0 }
	values.forEach( function(val) {
		reducedVal.totalWeight += val.totalWeight;
		reducedVal.totalHeight += val.totalHeight;
		reducedVal.count += val.count;
	});
	
	return reducedVal;
};

var finalizeFun = function (key, val) {
	return {
		avgWeight: val.totalWeight / val.count,
		avgHeight: val.totalHeight / val.count
	};
};

db.people.mapReduce( 
   mapFun, 
   reduceFun,
   { 
		out: "average_weight_height",
		finalize: finalizeFun
   }
);

printjson(db.average_weight_height.find().toArray())