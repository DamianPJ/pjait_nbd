var mapFun = function() {
	for (var i = 0; i < this.credit.length; i++) {
		emit(this.credit[i].currency, this.credit[i].balance);
	}
};

var reduceFun = function(key, values) {
	return Array.sum(values);
};

db.people.mapReduce( 
   mapFun, 
   reduceFun,
   { out: "total_balance_per_curr" }
);

printjson(db.total_balance_per_curr.find({}).toArray())

var mapFun = function() {
	this.credit.forEach(function(e) {
		emit(e.currency, e.balance);
	});
};

var reduceFun = function(key, values) {
	return Array.sum(values);
};

db.people.mapReduce( 
   mapFun, 
   reduceFun,
   { out: "total_balance_per_curr" }
);

printjson(db.total_balance_per_curr.find().toArray())