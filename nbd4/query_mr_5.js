var mapFun = function() {
	if (this.sex == "Female" && this.nationality == "Poland") {
		this.credit.forEach(c => {
			emit(c.currency, {totalBalance: c.balance, count: 1});
		});
	}
};

reduceFun = function(key, values) {
  reducedVal = {totalBalance: 0, count: 0};
  values.forEach(val => {
	reducedVal.totalBalance += val.totalBalance;
	reducedVal.count += val.count;
  });
  return reducedVal;
}

var finalizeFun = function (key, val) {
	return {
		avgBalance: val.totalBalance / val.count,
		total: val.totalBalance
	};
};

db.people.mapReduce( 
   mapFun, 
   reduceFun,
   { 
		out: "polish_women_balance",
		finalize: finalizeFun
   }
);

printjson(db.polish_women_balance.find().toArray())