var mapFun = function() {
	emit(this.nationality, {BMI: [(this.weight / Math.pow(this.height / 100, 2))], count: 1});
};

var reduceFun = function(key, values) {
  reducedVal = { BMI: [], count: 0};
  values.forEach(val => {
        reducedVal.BMI = reducedVal.BMI.concat(val.BMI)
	    reducedVal.count += val.count;
  });
  return reducedVal;
}

var finalizeFun = function (key, val) {
    var sum = 0, min = val.BMI[0], max = min;

    val.BMI.forEach(v => {
        if (v < min) min = v;
        if (v > max) max = v;
        sum += v;
    })

	return {
		avgBMI: sum / val.count,
        minBMI: min,
        maxBMI: max
	};
};

db.people.mapReduce( 
   mapFun, 
   reduceFun,
   { 
		out: "BMIs",
		finalize: finalizeFun
   }
);

printjson(db.BMIs.find().toArray())