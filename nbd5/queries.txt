1. MATCH (m:Movie) return m
2. MATCH (hugo:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(hugoMovies) return hugo, hugoMovies
3. MATCH (hugo:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(hugoMovies)<-[:DIRECTED]-(directors) return hugo, hugoMovies, directors
4. MATCH (hugo:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(hugoMovies)<-[:ACTED_IN]-(coactors) return hugo, hugoMovies, coactors
5. MATCH (m:Movie)<-[:ACTED_IN]-(p:Person)-[:ACTED_IN|DIRECTED|WROTE]->(mm:Movie) WHERE m.title CONTAINS "Matrix" RETURN mm, p, m
6. MATCH (actor:Person)-[:ACTED_IN]->(m:Movie) RETURN actor.name as Actor, COUNT(*) as MoviesCount
7. MATCH (m:Movie)<-[:DIRECTED]-(p:Person)-[:WROTE]->(m) RETURN p, m
8. MATCH (hugo:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(movies)<-[:ACTED_IN]-(keanu:Person {name: "Keanu Reeves"}) return hugo, keanu, movies
9. 
MERGE (p1:Person {name: "Hugo Weaving", born: 1960})
MERGE (p2:Person {name: "Chris Evans", born: 1981})
MERGE (p3:Person {name: "Samuel L. Jackson", born: 1948})
MERGE (p4:Person {name: "Joe Johnson", born: 1950})
MERGE (p5:Person {name: "Christopher Markus", born: 1970})
MERGE (p6:Person {name: "Stephen McFeely", born: 1969})
MERGE (m:Movie {title: "Captain America: The First Avenger", released: 2011, tagline: "When Patriots Become Heroes"})
MERGE (p1)-[:ACTED_IN]->(m)
MERGE (p2)-[:ACTED_IN]->(m)
MERGE (p3)-[:ACTED_IN]->(m)
MERGE (p4)-[:DIRECTED]->(m)
MERGE (p5)-[:WROTE]->(m)
MERGE (p6)-[:WROTE]->(m)


MATCH (m:Movie {title: "Captain America: The First Avenger"})<-[r]-(p:Person) return m, p