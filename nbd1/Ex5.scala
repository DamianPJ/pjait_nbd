package nbd1

object Ex5 {
  def main(args: Array[String]): Unit = {
    println(reducePrices(prices))
  }

  var prices = Map("Milk" -> 9.0, "Vine" -> 30.0, "Water" -> 1.0, "Cola" -> 5.5)
  val rate = 0.9

  def reducePrices(products: Map[String, Double]): Map[String, Double] = {
    products mapValues (_ * rate)
  }
}
