package nbd1

object Ex6 {

  def main(args: Array[String]): Unit = {
    printTuple(("Damian", 0.5, 1))
  }

  def printTuple(tuple: (String, Double, Int)) : Unit = {
    println("Custom: " + tuple._1 + " > " + tuple._2 + " > " + tuple._3)
    println("To string: " + tuple)
  }

}
