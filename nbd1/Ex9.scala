package nbd1

object Ex9 {
  def main(args: Array[String]): Unit = {
    print(incrementElements(List(1, 2, 3, 4, 5, 6)))
  }

  def incrementElements(list: List[Int]): List[Int] = {
    list map (_ + 1)
  }
}
