package nbd1


object Ex2 {

  def main(args: Array[String]): Unit = {
    println(joinToString(weekdays))
    println(joinToStringReversed(weekdays))
  }

  val weekdays = List("Monday",  "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
  val separator = ", "

  //a)
  def joinToString(list: List[String]): String = list match {
    case head :: tail =>
      if (tail.isEmpty) head
      else head + separator + joinToString(tail)
    case _ => ""
  }

  //b)
  def joinToStringReversed(list: List[String]): String = list match {
    case head :: tail =>
      if (tail.isEmpty) head
      else joinToStringReversed(tail) + separator + head
    case _ => ""
  }

}
