package nbd1

import scala.annotation.tailrec

object Ex8 {
  def main(args: Array[String]): Unit = {
    println(removeZerosRec(List(0, 1, 0, 2, 0, 3, 0, 4, 0)))
    println(removeZerosTailRec(List(0, 1, 0, 2, 0, 3, 0, 4, 0)))
  }

  def removeZerosRec(list: List[Int]): List[Int] = list match {
    case head :: tail => if (head != 0) head :: removeZerosRec(tail) else removeZerosRec(tail)
    case _ => List()
  }

  @tailrec
  def removeZerosTailRec(list: List[Int], result: List[Int] = List()): List[Int] = list match {
    case head :: tail => if (head != 0) removeZerosTailRec(tail, head :: result) else removeZerosTailRec(tail, result)
    case _ => result.reverse
  }
}
