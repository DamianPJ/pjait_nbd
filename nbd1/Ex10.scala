package nbd1

object Ex10 {

  def main(args: Array[String]): Unit = {
    print(Ex10.convert(List(-10, -7, -5, -2, 0, 3, 7, 12, 22, 42)))
  }

  def convert(list: List[Int]) : List[Int] = {
    list filter (-5 to 12 contains _) map (_.abs)
  }

}
