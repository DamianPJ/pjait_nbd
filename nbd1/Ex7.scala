package nbd1

object Ex7 {

  def main(args: Array[String]): Unit = {
    introduceOptions()
  }

  var capitals = Map("Poland" -> "Warsaw", "Germany" -> "Berlin", "England" -> "London", "France" -> "Paris", "Spain" -> "Madrid")

  def introduceOptions(): Unit = {
    println("Map: " + capitals)
    println("Element exists -> Some(Warsaw): " + capitals.get("Poland"))
    println("Element does not exist -> None: " + capitals.get("Austria"))
    println("Check if element is empty (No): " + capitals.get("Poland").isEmpty)
    println("Check if element is empty (Yes): " + capitals.get("Austria").isEmpty)
    println("Element exists unboxed: " + capitals.get("Poland").getOrElse("Unknown"))
    println("Element does not exist (default value): " + capitals.get("Austria").getOrElse("Unknown"))

//    Above can be shorten to this but I wanted to show Options clearly
//    println("Element exists unboxed: " + capitals.getOrElse("Poland", "Unknown"))
//    println("Element does not exist (default value): " + capitals.getOrElse("Austria", "Unknown"))
  }

}
