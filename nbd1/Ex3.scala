package nbd1

import scala.annotation.tailrec

object Ex3 {

  def main(args: Array[String]): Unit = {
    println(joinToStringTailRec(weekdays, ""))
  }

  val weekdays = List("Monday",  "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
  val separator = ", "

  @tailrec
  def joinToStringTailRec(list: List[String], accum: String): String = list match {
    case head :: tail =>
      if (tail.isEmpty) accum + separator + head
      else if (accum.isEmpty) joinToStringTailRec(tail, head)
      else joinToStringTailRec(tail, accum + separator + head)
    case _ => ""
  }
}
