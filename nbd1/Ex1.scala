package nbd1

/*
  I translated weekdays to polish, so point b) returns days that start with "T"
*/

object Ex1 {

  def main(args: Array[String]): Unit = {

    println(getStringWithFor(weekdays))
    println(getStringWithForFiltered(weekdays))
    println(getStringWithForFiltered2(weekdays))
    println(getStringWithWhile(weekdays))

  }

  val weekdays = List("Monday",  "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
  val separator = ", "

  //a)
  def getStringWithFor(list: List[String]): String = {

    var res = ""
    var iter = 0

    for (i <- list) {
      res += i

      if(iter != list.size - 1) {
        res += separator
      }

      iter += 1
    }

    res
  }

  //b)
  def getStringWithForFiltered(list: List[String]): String = {

    var res = ""

    for (i <- list if i.toLowerCase.startsWith("t")) {
      res += i + separator
    }

    //Drop handles empty list: slice(0, length - math.max(n, 0))
    res.dropRight(separator.length)
  }

  //Memory = O(N)
  def getStringWithForFiltered2(list: List[String]): String = {

    var filtered = List[String]()

    for (i <- list if i.toLowerCase().startsWith("t")) {
      filtered = filtered :+ i
    }

    getStringWithFor(filtered)
  }

  //c)
  def getStringWithWhile(list: List[String]): String = {

    var res = ""
    var iter = 0

    while (iter < list.size) {
      res += list(iter)

      if(iter != list.size - 1) {
        res += separator
      }

      iter += 1
    }

    res
  }
}