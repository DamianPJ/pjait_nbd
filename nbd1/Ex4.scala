package nbd1

/*
  I translated weekdays to polish, so point c) returns days that start with "T"
*/

object Ex4 {

  def main(args: Array[String]): Unit = {
    println(joinToStringFoldl(weekdays))
    println(joinToStringFoldr(weekdays))
    println(joinToStringFoldr2(weekdays))
    println(joinToStringFoldlFiltered(weekdays))
  }

  val weekdays = List("Monday",  "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
  val separator = ", "

  //a)
  def joinToStringFoldl(list: List[String]): String = {
    list.tail.foldLeft(list.head) (_ + separator + _)
  }

  //b)
  def joinToStringFoldr(list: List[String]): String = {
    list.foldRight("") ((a, b) => if (b.isEmpty) a else a + separator + b)
  }

  def joinToStringFoldr2(list: List[String]): String = {
    list.foldRight("") (_ + separator + _).dropRight(separator.length)
  }

  //c)
  def joinToStringFoldlFiltered(list: List[String]): String = {
    list.foldLeft("") ((a, b) =>
      if (b.startsWith("T") && a.isEmpty) b
      else if (b.startsWith("T") && a.nonEmpty) a + separator + b
      else a
    )
  }
}
