package nbd2

object Ex3 {

  def main(args: Array[String]): Unit = {
    val p1 = Person("John", "Smith")
    val p2 = Person("Bill", "Frank")
    val p3 = Person("Mark", "Spark")
    val p4 = Person("Edgar", "Evernever")

    println(welcome(p1))
    println(welcome(p2))
    println(welcome(p3))
    println(welcome(p4))
  }

  def welcome(person: Person) = person match {
    case Person(name, "Frank") => s"$name Frank? Which one is your name?"
    case Person(_, "Smith") => "Eh, one more Smith..."
    case Person("Mark", _) => "Mark? Like Mark Zuckerberg!"
    case Person(name, _) => s"Hi, $name."
  }
}


case class Person(var name: String, var surname: String)
