package nbd2

object Ex4 {
  def main(args: Array[String]): Unit = {
    println(triple(2, _ * 2))
    println(triple(4, _ + 2))
  }

  def triple(n: Int, f: Int => Int): Int = f(f(f(n)))

}
