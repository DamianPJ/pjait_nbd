package nbd2

object Ex1 {
  def main(args: Array[String]): Unit = {
    val weekdays = List("Monday",  "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Goose")

    for(i <- weekdays)
      println(i + ": " + checkDay(i))
  }

  def checkDay(day: String): String = day match {
    case "Monday" |  "Tuesday" | "Wednesday" | "Thursday" | "Friday" => "Work"
    case "Saturday" | "Sunday" => " Weekend"
    case _ => "No such a day"
  }

}
