package nbd2

object Ex5 {
  def main(args: Array[String]): Unit = {
    val student = new Person("Student", "Student") with Student
    val teacher = new Person("Teacher", "Teacher") with Teacher
    val employee = new Person("Employee", "Employee") with Employee

    val studyingEmployee = new Person("StudyingWorker", "StudyingWorker") with Student with Employee
    val workingStudent = new Person("WorkingStudent", "WorkingStudent") with Employee with Student

    teacher.salary_(1000)
    employee.salary_(1000)
    studyingEmployee.salary_(1000)
    workingStudent.salary_(1000)

    println(student.name + " pays " + student.tax)
    println(teacher.name + " pays " + teacher.tax)
    println(employee.name + " pays " + employee.tax)
    println(studyingEmployee.name + " pays " + studyingEmployee.tax)
    println(workingStudent.name + " pays " + workingStudent.tax)
  }
}

abstract class Person(val name: String, val surname: String) {
  def tax: Double
}

trait Student extends Person {
  override def tax = 0.0
}

trait Teacher extends Employee {
  override def tax = salary * 0.1
}

trait Employee extends Person {
  override def tax = salary * 0.2
  private var sal: Double = _
  def salary = sal
  def salary_(s: Double): Unit = sal = s
}