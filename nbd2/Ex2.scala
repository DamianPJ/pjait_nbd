package nbd2

object Ex2 {
  def main(args: Array[String]): Unit = {
    val account1 = new BankAccount(1000)
    val account2 = new BankAccount()

    println(account1.balance)
    println(account2.balance)

    account1.withdrawal(100)
    account2.deposit(10000)

    println(account1.balance)
    println(account2.balance)
  }
}

class BankAccount(private var _balance: Double) {

  def this() {
    this(0)
  }

  def balance = _balance

  def deposit(amount: Double): Unit = {
    if (amount > 0) {
      _balance += amount
    } else throw new Exception("Incorrect amount")
  }

  def withdrawal(amount: Double): Unit = {
    if (amount > 0 && amount < _balance) {
      _balance -= amount
    } else throw new Exception("Incorrect amount")
  }
}