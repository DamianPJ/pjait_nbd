import riak

client = riak.RiakClient()

bucket = client.bucket('pyBucket')

# Create, fetch and display
print('Create, fetch and display')

person = bucket.new('doc1', data={
    'name': 'Damian',
    'age': 23
})

person.store()

fetchedPerson = bucket.get('doc1')

print(fetchedPerson.data)

# Modify, fetch and display
print('Modify, fetch and display')

fetchedPerson.data['name'] = 'Piotr'

fetchedPerson.store()

fetchedPerson2 = bucket.get('doc1')

print(fetchedPerson2.data)

# Delete, fetch and display
print('Delete, fetch and display')

fetchedPerson2.delete()

nonExistingPerson = bucket.get('doc1')

print(nonExistingPerson.data)
