printjson(
    db.people.insert(
        {
            "sex": "Male",
            "first_name": "Damian",
            "last_name": "Kalicki",
            "job": "Awesome Java Dev",
            "email": "s13828@pjatk.edu.pl",
            "location": {
                "city": "Warsaw",
                "address": {
                    "streetname": "Koszykowa",
                    "streetnumber": "86"
                }
            },
            "description": "Objct added in NBD3 Ex 6",
            "height": 191.00,
            "weight": 68.37,
            "birth_date": "1996-01-01T16:10:58Z",
            "nationality": "Poland",
            "credit": [
                {
                    "type": "BNP",
                    "number": "4017957170327",
                    "currency": "PLN",
                    "balance": 4463.86
                }
            ]
        }
    )
)